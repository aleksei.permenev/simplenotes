package ru.tripplea.simplenotes;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import ru.tripplea.simplenotes.database.DatabaseHelper;
import ru.tripplea.simplenotes.database.model.Note;

/**
 * main class of main activity
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        reload();
    }

    /**
     * Refresh scrollview data
     */
    public void reload() {
        LinearLayout scrollView = findViewById(R.id.scrollViewLayout);
        scrollView.removeAllViews();
        FragmentManager fragMan = getFragmentManager();
        FragmentTransaction fragTransaction = fragMan.beginTransaction();

        DatabaseHelper db = new DatabaseHelper(this);
        for (Note note : db.getAllNotes()) {
            Fragment noteFragment = NoteFragment.newInstance(note);
            fragTransaction.add(R.id.scrollViewLayout, noteFragment, "fragment" + scrollView.getChildCount());
        }

        fragTransaction.commit();
    }

    /**
     * Event to click button for create new note
     * @param view sender object
     */
    public void onNewNote(View view) {
        Note note = new Note("", "");
        DialogFragment dialog = CreateNoteDialog.createDialog(note);
        dialog.show(getFragmentManager(), "NoticeDialogFragment");
    }

    /**
     * Event to click button for get time
     * @param view sender object
     * @throws ExecutionException exception of non-main-thread task
     * @throws InterruptedException exception of non-main-thread task
     * @throws JSONException exception of json deserialization
     */
    public void onGetTime(View view) throws ExecutionException, InterruptedException, JSONException {
        JSONObject json = new RetrieveFeedTask().execute("https://worldtimeapi.org/api/timezone/Europe/Samara").get();
        String[] ar = json.get("datetime").toString().split("[T+.]");
        Toast.makeText(getApplicationContext(), "Today is " + ar[0] + " " + ar[1], Toast.LENGTH_LONG).show();
    }
}
